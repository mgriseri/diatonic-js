import { assert } from 'chai'
import { noteFromStr, noteToStr } from '../src/diatonic.js'

describe("Tests for noteFuncs.js", function() {
  let expected = {
    "C": [0, 0],
    "C#": [0, 1],
    "Gb": [4, -1],
    "Bb": [6, -1],
  }

  describe("Tests for noteFromStr()", function() {
    for (let [noteName, noteRepr] of Object.entries(expected)) {
      it(`should equals ${noteRepr} when called with ${noteName}`, function() {
        assert.deepEqual(noteFromStr(noteName), noteRepr)
      })
    }

    it("should throw an error if argument is falsy or isn't a string", function() {
      assert.throws(function() { noteFromStr("") }, "Invalid note")
      assert.throws(function() { noteFromStr(["a", "b"]) }, "Invalid note")
      assert.throws(function() { noteFromStr([]) }, "Invalid note")
      assert.throws(function() { noteFromStr(null) }, "Invalid note")
      assert.throws(function() { noteFromStr(undefined) }, "Invalid note")
    })

    it("should throw an error when called with non-existent note names", function() {
      assert.throws(function() { noteFromStr("Z") }, "Invalid tone")
      assert.throws(function() { noteFromStr("K#") }, "Invalid tone")
    })
    it("should throw an error when argument has invalid alteration", function() {
      assert.throws(function() { noteFromStr("C$") }, "Invalid alteration")
      assert.throws(function() { noteFromStr("C#b") }, "Invalid alteration")
    })
  })

  describe("Tests for noteToStr()", function() {
    for (let [noteName, noteRepr] of Object.entries(expected)) {
      it(`should equals ${noteName} when called with ${noteRepr}`, function() {
        assert.deepEqual(noteToStr(noteRepr), noteName)
      })
    }

    it("should throw an error when called with wrong note representation", function() {
      assert.throws(function() { noteToStr([0, 0, 0]) }, "Invalid note")
      assert.throws(function() { noteToStr([-1, 0]) }, "Invalid note")
      assert.throws(function() { noteToStr([7, 0]) }, "Invalid note")
      assert.throws(function() { noteToStr([]) }, "Invalid note")
    })
  })
}
)
