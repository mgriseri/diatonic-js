import { assert } from 'chai'
import { intervalFromStr, intervalToStr } from '../src/diatonic.js'
import { addInterval, findInterval } from '../src/diatonic.js'
import { noteFromStr, noteToStr } from '../src/diatonic.js'

describe("Tests for intervFuncs.js", function() {
  describe("Tests for intervalFromStr", function() {
    const expected = {
      "P1": [0, 0],
      "M3": [2, 4],
      "P4": [3, 5],
      "M7": [6, 11]
    }

    for (let [intervName, intervRep] of Object.entries(expected)) {
      it(`should return ${intervRep} when called with ${intervName}`,
      function() {
        assert.deepEqual(intervRep, intervalFromStr(intervName))
      })
    }
  })
  describe("Tests for intervalToStr", function() {
    const expected = {
      "P1": [0, 0],
      "M3": [2, 4],
      "P4": [3, 5],
      "M7": [6, 11]
    }

    for (let [intervName, intervRep] of Object.entries(expected)) {
      it(
        `should return ${intervName} when called with ${intervRep}`,
        function() {
          assert.deepEqual(intervName, intervalToStr(intervRep))
        }
      )
    }

    it(
      "should throw an error when arg isnt a 2 character str",
      function() {
        assert.throws(function() { intervalFromStr("BlaBlaBla") })
      }
    )

    it(
      "should throw an error when arg is falsy",
      function() {
        assert.throws(function() { intervalFromStr("") })
        assert.throws(function() { intervalFromStr(null) })
        assert.throws(function() { intervalFromStr(undefined) })
      }
    )
  })

  describe("Tests for addInterval", function() {
    const expected = [
      ["C", "M3", "E"],
      ["C", "P4", "F"],
      ["G", "P4", "C"],
      ["B", "P5", "F#"],
      ["B#", "P5", "F##"],
      ["A#", "P5", "E#"],
      ["C", "S", "Db"],
      ["C", "m2", "Db"],
      ["C", "T", "D"],
      ["C", "M2", "D"],
      ["C", "TT", "Gb"],
      ["C", "d5", "Gb"],
      ["C#", "T", "D#"],
      ["C#", "M2", "D#"],
      ["F#", "T", "G#"],
      ["F#", "M2", "G#"],
      ["B", "S", "C"],
      ["B", "m2", "C"],
    ]

    for (let [note, interval, result] of expected) {
      it(
        `should return ${result} note when called with ${note}, ${interval}`,
        function() {
          assert.equal(
            result,
            noteToStr(addInterval(noteFromStr(note), intervalFromStr(interval)))
          )
        }
      )
    }
  })

  describe("Tests for findInterval", function() {
    const expected = [
      ["C", "M3", "E"],
      ["C", "P4", "F"],
      ["G", "P4", "C"],
      ["B", "P5", "F#"],
      ["B#", "P5", "F##"],
      ["A#", "P5", "E#"],
      ["C", "m2", "Db"],
      ["C", "M2", "D"],
      ["C", "d5", "Gb"],
      ["C#", "M2", "D#"],
      ["F#", "M2", "G#"],
      ["B", "m2", "C"],
      ["B", "S", "B#"],
      ["B", "T", "B##"],
      ["Bb", "TT", "D##"],
    ]

    for (let [note1, interval, note2] of expected) {
      it(
        `should return ${interval} interval when called with ${note1}, ${note2}`,
        function() {
          assert.equal(
            interval,
            intervalToStr(findInterval(noteFromStr(note1), noteFromStr(note2)))
          )
        }
      )
    }
  })
})