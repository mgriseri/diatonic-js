import { assert } from 'chai'
import { noteFromStr, nameAtChromaIndex, simpleNameFlats, simpleNameSharps} from '../src/diatonic.js'

describe("Tests for nameFuncs", function() {
  describe("Tests for nameAtChromaIndex", function() {
    const expected = [
      ["C", 0, "C"],
      ["C", 1, "C#"],
      ["C", 11, "Cb"],
      ["G", 6, "Gb"],
      ["G", 7, "G"],
      ["G", 8, "G#"],
    ]

    for (let [name, chromaIndex, expName] of expected) {
      it(`should return ${expName} when called with ${name}, ${chromaIndex}`,
      function() {
        assert.deepEqual(
          expName,
          nameAtChromaIndex(noteFromStr(name), chromaIndex))
      })
    }
  })

  describe("Tests for simpleNameFlats", function() {
    const expected = {
      "C": "C",
      "F#": "Gb",
      "B#": "C",
      "B##": "Db",
    }
    for (let [name, simpleName] of Object.entries(expected)) {
      it(`should equal ${simpleName} when called with ${name}`, function() {
        assert.equal(
          simpleName,
          simpleNameFlats(noteFromStr(name), simpleName))
      })
    }
  })

  describe("Tests for simpleNameSharps", function() {
    const expected = {
      "C": "C",
      "Gb": "F#",
      "B#": "C",
      "B##": "C#",
    }
    for (let [name, simpleName] of Object.entries(expected)) {
      it(`should equal ${simpleName} when called with ${name}`, function() {
        assert.equal(
          simpleName,
          simpleNameSharps(noteFromStr(name), simpleName)
        )
      })
    }
  })
})
