import { assert } from 'chai'
import { noteFromStr, chromaIndex, chromaIndexFrom } from '../src/diatonic.js'

describe("Tests for noteFuncs.js", function() {
  describe("Tests for chromaIndex", function() {
    const expected = {
      "C": 0,
      "C############": 0,
      "Cbbbbbbbbbbbb": 0,
      "C#": 1,
      "F": 5,
      "F############": 5,
      "Fb": 4,
      "Fbb": 3,
      "G": 7,
      "Bb": 10,
      "B": 11,
      "B#": 0,
    }
    for (let [noteName, noteChromaIndex] of Object.entries(expected)) {
      it(
        `should return ${noteChromaIndex} when called with ${noteName}`,
        function() {
          assert.equal(
            chromaIndex(noteFromStr(noteName)),
            noteChromaIndex
          )
        })
    }
  })
  describe("Tests for chromaIndexFrom from A Natural", function() {
    const expected = {
      "C": 3,
      "C############": 3,
      "Cbbbbbbbbbbbb": 3,
      "C#": 4,
      "F": 8,
      "F############": 8,
      "Fb": 7,
      "Fbb": 6,
      "G": 10,
      "Bb": 1,
      "B": 2,
      "B#": 3,
    }
    for (let [noteName, noteChromaIndex] of Object.entries(expected)) {
      it(
        `should return ${noteChromaIndex} when called with ${noteName}`,
        function() {
          assert.equal(
            chromaIndexFrom(noteFromStr("A"), noteFromStr(noteName)),
            noteChromaIndex
          )
        })
    }
  })
})