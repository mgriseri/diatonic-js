# diatonic.js

**diatonic.js** is a tiny JavaScript library
to manipulate musical notes, intervals and scales.

## How to use

Import the following file (like any other JS library) in your project:

    https://gitlab.com/mgriseri/diatonic-js/raw/master/dist/diatonic.js

Read the [documentation](https://gitlab.com/mgriseri/diatonic-js/blob/master/DOCS.md).

## How to build

First, clone the repo and install dependencies:

    git clone https://gitlab.com/mgriseri/diatonic-js
    cd diatonic-js
    npm install

Then, build with:

    npm run build

## How to run the tests

If you just want to run the tests (without building a new bundle):

    npm run test
