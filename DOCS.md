# diatonic.js documentation

## Functions

### Note functions

#### noteFromStr

    noteFromStr(STRING) => NOTE

The **noteFromStr** function takes a *string* and returns an *array*.

This array represents a musical note:

- The **first number** is the **index** of the note on the C major scale.
- The **second number** is its **alteration**.

For example, *C* is the first note of the C major scale and has no alteration.

Therefore, its representation is: `[0, 0]`

```javascript
noteFromStr("C")  // [0, 0]
```

*C#* however, has the same index but has one sharp,
so the representation is: `[0, 1]`

```javascript
noteFromStr("C#")  // [0, 1]
```

Similarly, *Cb* is still the first note of the C major scale,
but with one flat (one negative alteration).

```javascript
noteFromStr("Cb")  // [0, -1]
```

Since we start counting at zero, the indexes are:

    0    1    2    3    4    5    6
    C    D    E    F    G    A    B

#### noteToStr

    noteToStr(NOTE) => STRING

The **noteToStr** function is the reciprocal of **noteFromStr**.

It takes an *array* (that represents a note) and returns a *string*.

Basically, it converts any note to a **human friendly**, readable string.

```javascript
// Human to computer
noteFromStr("G")  // [4, 0]
// Computer to human
noteToStr([4, 0])  // "G"
```

#### chromaIndex

    chromaIndex(NOTE) => NUMBER

As the name suggests, **chromaIndex** takes an *array* representing a note and returns
a *number* between 0 and 11.

This number is the **index of the note on the C chromatic scale**.

For example, *C* is the first note of the C chromatic scale:

```javascript
chromaIndex(noteFromStr("C"))  // 0
```

And G is at index 7:

```javascript
chromaIndex(noteFromStr("G"))  // 7
```

*C#* and *Db* are **spelled differently** but have **the same index** on a C chromatic  scale:

```javascript
chromaIndex(noteFromStr("C#"))  // 1
chromaIndex(noteFromStr("Db"))  // 1
```

This is true for both *E#* and *F* as well:

```javascript
chromaIndex(noteFromStr("E#"))  // 5
chromaIndex(noteFromStr("F"))  // 5
```

Again, since we start counting at 0, the indexes are:

    0    1    2    3    4    5    6    7    8    9    10    11
    C    Db   D    Eb   E    F    Gb   G    Ab   A    Bb    B



#### chromaIndexFrom

**chromaIndexFrom** behaves in the same way as **chromaIndex**,
but starting from a given note.

    chromaIndexFrom(STARTING_NOTE, NOTE) => NUMBER

For example,

On the *C* chromatic scale, *C* is at index *0*.

On the *A* chromatic scale, *C* is at index *3*.

```javascript
const startingNote = noteFromStr("A")
const someNote = noteFromStr("C")
chromaIndexFrom(startingNote, someNote)  // 3
```

You can convince yourself this is true since *C* is
on the *3rd fret* of the *A* string of a guitar.

#### nameAtChromaIndex

    nameAtChromaIndex(NOTE, NUMBER) => STRING

This function takes an note and a number and returns a string.

It answers the question: "How would this note be spelled at that specific chromatic index?"

For instance, we know *C* is at index 0 on the C chromatic scale.

So, its name at index 1 would be *C#*. At index 2, it would be *C##*, etc.

```javascript
const myNote = noteFromStr("C")
nameAtChromaIndex(myNote, 0)  // "C"
nameAtChromaIndex(myNote, 1)  // "C#"
nameAtChromaIndex(myNote, 2)  // "C##"
```

However, at index 11, it should be named *Cb*, not *C###########* (for obvious sanity reasons).

```javascript
const myNote = noteFromStr("C")
nameAtChromaIndex(myNote, 11)  // "Cb"
```

Here is another example:

We know *F* and *E#* are enharmonic equivalents.

```javascript
const fNatural = noteFromStr("F")
const eNatural = noteFromStr("E")
const indexOfFNatural = chromaIndex(fNatural)
nameAtChromaIndex(eNatural, indexOfFNatural)  // "E#"
```

The last line means:
"The name of *E* at the same index as *F* is *E#*."

#### simpleNameFlats and simpleNameSharps

    simpleNameFlats(NOTE) => STRING
    simpleNameSharps(NOTE) => STRING


Those functions are pretty self-explanatory.
They take a note and return its simplest spelling as a string.

You can easily switch between sharp and flat notation

Examples:

*D#* and *Eb* are enharmonic equivalents:

```javascript
simpleNameFlats(noteFromStr("D#"))  // "Eb"
simpleNameSharps(noteFromStr("Eb"))  // "D#"
```

The same goes for *B#* and *C*

```javascript
simpleNameFlats(noteFromStr("B#"))  // "C"
```

### Interval functions

#### intervalFromStr

The **intervalFromStr** function takes a string and returns an interval.
An interval in an array containing 2 numbers:

- The **first number** is a number of **diatonic notes**
- The **second number** is a number of **semitones**

For example, a perfect fifth is 7 semitones away from its root.
But its called a *fifth*, which means its diatonic number is *5*.

Since we start counting at zero, we could write:

```javascript
const perfectFifth = [4, 7]
```

You can read the last line as:

- "A perfect fifth is named after the **4th note** away from its root"
- "A perfect fifth is **7 semitones** away from its root."


Using intervalFromStr, you can write

```javascript
const perfectFifth = intervalFromStr("P5")
```

Instead of

```javascript
const perfectFifth = [4, 7]
```

The possible intervals are:

- "P1"  Perfect Unison
- "m2"  Minor Second
- "M2"  Major Second
- "m3"  Minor Third
- "M3"  Major Third
- "P4"  Perfect Fourth
- "A4"  Augmented Fourth
- "d5"  Diminished Fifth
- "P5"  Perfect Fifth
- "A5"  Augmented Fifth
- "m6"  Minor Sixth
- "M6"  Major Sixth
- "d7"  Diminished Seventh
- "m7"  Minor Seventh
- "M7"  Major Seventh

Along with 3 intervals without a diatonic number

- "S"   Semitone
- "T"   Tone
- "TT"  Tritone

Those last three intervals do not have a name (diatonic number).
Therefore, the simplest name (using flats) will be chosen.

#### intervalToStr

    intervalToStr(INTERVAL) => STRING

This is the reciprocal of **intervalFromStr**.
It takes an interval (array of two numbers) and returns a human friendly string.

Examples:

Interval created manually

```javascript
const perfectFifth = [4, 7]
intervalToStr(perfectFifth)  // "P5"
```

Interval created with intervalFromStr

```javascript
const perfectFourth = intervalFromStr("P4")
intervalToStr(perfectFourth)  // "P4"
```

#### addInterval

    addInterval(NOTE, INTERVAL) => NOTE

**addInterval** takes a note and an interval and returns a new note.
It answers questions like:

- What is the perfect fifth of C?
- What is the minor third of F?

Examples:

```javascript
const myRootNote = noteFromStr("C")
const myInterval = intervalFromStr("P5")
const myNewNote = addInterval(myRootNote, myInterval)

noteToStr(myNewNote)  // "G"
```

Note that **addInterval** does not accept strings,
we need to use **noteFromStr** and **intervalFromStr**
to pass the note and interval properly.

Similarly, we need to use **noteToSt**
to see the output in a human friendly way.

#### findInterval

    findInterval(NOTE1, NOTE2) => INTERVAL

It takes two notes and returns the corresponding interval.

It answers questions like:

- Which interval is *C* followed by *F*?
- Which interval is *G* followed by *B*?

Or, in a general way:

- Which interval is *NOTE1* followed by *NOTE2*?

Using **findInterval**, we can quickly get those answers:

```javascript
const cNatural = noteFromStr("C")
const fNatural = noteFromStr("F")
const myInterval = findInterval(cNatural, fNatural)

console.log(intervalToStr(myInterval))
// "P4"
```

```javascript
const gNatural = noteFromStr("G")
const bNatural = noteFromStr("B")
const myInterval = findInterval(gNatural, bNatural)

console.log(intervalToStr(myInterval))
// "M3"
```

### Scale functions

#### scale

    scale(NOTE, INTERVAL_ARRAY) => SCALE

- The first argument should be a note (the root note).
- The second should be an array of intervals (the scale definition).
- The output is an array of notes we call 'scale'.

This function is useful when you want to create your own scale,
and see which notes are in it.

For example, imagine the following scale:

    C   D   E   G   A

This is actually the C major pentatic scale and
it is composed of the following intervals:

    P1  M2  M3  P5  M6

We could tweak this scale and create our own spicy version of it:

    P1  M2  M3  P5  M6  m7

There is probably a name for this scale...
Or you could think of it as a '6/9 add m7' arpeggio or something.

But who cares? It is just a custom scale, so we could write:

```javascript
const customScale = [
  intervalFromStr("P1"),
  intervalFromStr("M2"),
  intervalFromStr("M3"),
  intervalFromStr("P5"),
  intervalFromStr("M6"),
  intervalFromStr("m7"),
]
```

Or in a shorter way, using map:

```javascript
const customScale = ["P1", "M2", "M3", "P5", "M6", "m7"].map(
  n => intervalFromStr(n)
)
```

To get the notes in this scale for a given root note,
use the **scale** function:

```javascript
const rootNote = noteFromStr("C")
const resultsAsNotes = scale(rootNote, customScale)
```

Finally, we can apply **noteToSt** to every element in resultsAsNotes
to store the results as strings.

```javascript
const resultsAsStrings = resultsAsNotes.map(n => noteToStr(n))
console.log(resultsAsStrings)
// ["C", "D", "E", "G", "A", "Bb"]
```

#### scaleStack

    scaleStack(NOTE, INTERVAL_ARRAY) => SCALE

**scaleStack** is pretty much the same as **scale** except it will
compute the next interval from the last note instead of the root note.

For example, consider this *scale*:

    C   E   G

You may have noticed this is actually a *C major* arpeggio.

We could describe this sequence of notes in two different ways:

- Take the note *C*, add its *major third* and its *perfect fifth*

Or, by applying intervals to successively:

- Take the note *C*, and stack a *major third* and a *minor third*.

Both descriptions are correct, remember:

- The *minor third* of a *major third* is a *perfect fifth*.
- The *major third* of a *minor third* is a *perfect fifth*.

Depending on which way you want to describe the scale / arpeggio
of the day, you could use use **scale** or **scaleStack**.

Here is another example:

If you want a stack of 3 minor thirds starting from C, you can do:

```javascript
const myRootNote = noteFromStr("C")
const myStack = ["P1", "m3", "m3", "m3"].map(n => intervalFromStr(n))
const myScale = scaleStack(myRootNote, myStack)

const myScaleAsStrings = myScale.map(n => noteToStr(n))
console.log(myScaleAsStrings)
// ["C", "Eb", "Gb", "Bbb"]
```

Which is correct, because we apply the intervals like so:

- The *perfect unison* of C is **C**
- The *minor third* of C is **Eb**
- The *minor third* of Eb is **Gb**
- The *minor third*  of Gb is **Bbb**

These are the notes in a *C dim7* chord, which is indeed a stack of minor thirds.

#### scalePreset

    scalePreset(NOTE, STRING) => SCALE

Use this function when dealing with common scales.
Just like **scale** and **scaleStack**, this function returns an array of notes.

With it, you can just pass strings like "major" or "minor" instead
of constructing the major or minor scale manually.

Examples:

```javascript
const myRootNote = noteFromStr("C")
const cMajorScale = scalePreset(myRootNote, "major")

const cMajorScaleAsStrings = cMajorScale.map(n => noteToStr(n))
console.log(cMajorScaleAsStrings)
// ["C", "D", "E", "F", "G", "A", "B"]
```

The possible scale presets are:

- "major"
- "harmMajor"
- "minor"
- "harmMinor"
- "meloMinor"

Along with the seven diatonic modes:

- "ionian"
- "dorian"
- "phrygian"
- "lydian"
- "mixolydian"
- "aeolian"
- "locrian"
