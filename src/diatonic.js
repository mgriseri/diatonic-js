import { noteFromStr, noteToStr } from './noteFuncs.js'
import { chromaIndex, chromaIndexFrom } from './chromaFuncs.js'
import { nameAtChromaIndex, simpleNameFlats, simpleNameSharps } from './nameFuncs.js'
import { intervalFromStr, intervalToStr, addInterval, findInterval } from './intervFuncs.js'
import { scale, scaleStack, scalePreset, sortNotes, findTonic } from './scaleFuncs.js'
import { chordPreset } from './chordFuncs.js'

export {
  noteFromStr,
  noteToStr,
  chromaIndex,
  chromaIndexFrom,
  nameAtChromaIndex,
  simpleNameFlats,
  simpleNameSharps,
  intervalFromStr,
  intervalToStr,
  addInterval,
  findInterval,
  scale,
  scaleStack,
  scalePreset,
  sortNotes,
  findTonic,
  chordPreset
}

export default {
  "noteFromStr": noteFromStr,
  "noteToStr": noteToStr,
  "chromaIndex": chromaIndex,
  "chromaIndexFrom": chromaIndexFrom,
  "nameAtChromaIndex": nameAtChromaIndex,
  "simpleNameFlats": simpleNameFlats,
  "simpleNameSharps": simpleNameSharps,
  "intervalFromStr": intervalFromStr,
  "intervalToStr": intervalToStr,
  "addInterval": addInterval,
  "findInterval": findInterval,
  "scale": scale,
  "scaleStack": scaleStack,
  "scalePreset": scalePreset,
  "sortNotes": sortNotes,
  "findTonic": findTonic,
  "chordPreset": chordPreset,
}
