import { noteToStr } from './noteFuncs.js'
import { chromaIndex } from './chromaFuncs.js'

const nameAtChromaIndex = (note, newChromaIndex) => {
  const toneDiatonicIndex = note[0], alteration = note[1]
  const noteChromaIndex = chromaIndex(note)

  let newAlt = newChromaIndex - noteChromaIndex + alteration

  if (Math.abs(newAlt) >= 6) {
    let solutions = [newAlt - 12, newAlt + 12]
    solutions.sort((x, y) => Math.abs(x) - Math.abs(y))
    newAlt = solutions[0]
  }

  return noteToStr([toneDiatonicIndex, newAlt])
}

const simpleNameFlats = (note) => {
  const chromaScale = [
    "C", "Db", "D", "Eb",
    "E", "F", "Gb", "G",
    "Ab", "A", "Bb", "B"
  ]

  return chromaScale[chromaIndex(note)]
}

const simpleNameSharps = (note) => {
  const chromaScale = [
    "C", "C#", "D", "D#",
    "E", "F", "F#", "G",
    "G#", "A", "A#", "B"
  ]

  return chromaScale[chromaIndex(note)]
}

export {
  nameAtChromaIndex,
  simpleNameFlats,
  simpleNameSharps
}
