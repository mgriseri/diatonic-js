const chordRules = {
  // TRIADS
  "maj": ["P1", "M3", "P5"],
  "min": ["P1", "m3", "P5"],
  "aug": ["P1", "M3", "A5"],
  "dim": ["P1", "m3", "d5"],
  // SEVENTH CHORDS
  // Basic
  "maj7": ["P1", "M3", "P5", "M7"],
  "7": ["P1", "M3", "P5", "m7"],
  "min7": ["P1", "m3", "P5", "m7"],
  // Augmented
  "aug7": ["P1", "M3", "A5", "m7"],
  "augMaj7": ["P1", "M3", "A5", "M7"],
  // Diminished
  "m7b5": ["P1", "m3", "d5", "m7"],
  "dim7": ["P1", "m3", "d5", "d7"],
}

const diatonicNotes = {
  'C': 0,
  'D': 2,
  'E': 4,
  'F': 5,
  'G': 7,
  'A': 9,
  'B': 11,
}

const diatonicScale = Object.keys(diatonicNotes)

const intervalRules = {
  "P1": [0, 0],
  "S": [1],
  "m2": [1, 1],
  "T": [2],
  "M2": [1, 2],
  "m3": [2, 3],
  "M3": [2, 4],
  "P4": [3, 5],
  "TT": [6],
  "A4": [3, 6],
  "d5": [4, 6],
  "P5": [4, 7],
  "A5": [4, 8],
  "m6": [5, 8],
  "M6": [5, 9],
  "d7": [6, 9],
  "m7": [6, 10],
  "M7": [6, 11],
}

const scaleRules = {
  "major": ["P1", "M2", "M3", "P4", "P5", "M6", "M7"],
  "harmMajor": ["P1", "M2", "M3", "P4", "P5", "m6", "M7"],
  "minor": ["P1", "M2", "m3", "P4", "P5", "m6", "m7"],
  "harmMinor": ["P1", "M2", "m3", "P4", "P5", "m6", "M7"],
  "meloMinor": ["P1", "M2", "m3", "P4", "P5", "M6", "M7"],
  "ionian": ["P1", "M2", "M3", "P4", "P5", "M6", "M7"],
  "dorian": ["P1", "M2", "m3", "P4", "P5", "M6", "m7"],
  "phrygian": ["P1", "m2", "m3", "P4", "P5", "m6", "m7"],
  "lydian": ["P1", "M2", "M3", "A4", "P5", "M6", "M7"],
  "mixolydian": ["P1", "M2", "M3", "P4", "P5", "M6", "m7"],
  "aeolian": ["P1", "M2", "m3", "P4", "P5", "m6", "m7"],
  "locrian": ["P1", "m2", "m3", "P4", "d5", "m6", "m7"],
  "pentaMajor": ["P1", "M2", "M3", "P5", "M6"],
  "pentaMinor": ["P1", "m3", "P4", "P5", "m7"]
}

export {
  chordRules,
  diatonicNotes,
  diatonicScale,
  intervalRules,
  scaleRules
}
