import { chordRules } from './constants.js'
import { intervalFromStr } from './intervFuncs.js'
import { scale } from './scaleFuncs.js'

const chordPreset = (note, chordName) => {
  return scale(note, chordRules[chordName].map(n => intervalFromStr(n)))
}

export { chordPreset }
