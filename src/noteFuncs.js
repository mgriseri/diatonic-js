import { diatonicScale } from './constants.js'

const noteFromStr = (str) => {
  const isNonEmptyStr = (str && typeof(str) === "string")

  if (!isNonEmptyStr) {
    throw new Error("Invalid note passed to noteFromStr.\n")
  }

  const tone = str.slice(0, 1)
  const rest = str.slice(1, str.length)

  if (!diatonicScale.includes(tone)) {
    throw new Error(`Invalid tone: ${tone} not in ${diatonicScale}`)
  }

  const hasInvalidAlter = str.length > 1 && !(rest.match("#") || rest.match("b"))
  const hasBothSharpsFlats = rest.match("#") && rest.match("b")

  if (hasInvalidAlter || hasBothSharpsFlats) {
    throw new Error("Invalid alteration pattern")
  }

  const toneDiatonicIndex = diatonicScale.indexOf(tone)

  const nbOfSharps = str.split("#").length - 1
  const nbOfFlats = str.split("b").length - 1

  const isFlat = str[str.length - 1] === "b"
  const isSharp = str[str.length - 1] === "#"

  let output = null

  if (isFlat) {
    output = [toneDiatonicIndex, -1 * nbOfFlats]
  } else if (isSharp) {
    output = [toneDiatonicIndex, nbOfSharps]
  } else {
    output = [toneDiatonicIndex, 0]
  }

  return output
}

const noteToStr = (note) => {
  const isArrayOfTwo = Array.isArray(note) && note.length === 2  
  const hasInvalidTone = note[0] < 0 || note[0] > 6

  if (!isArrayOfTwo || hasInvalidTone) {
    throw new Error("Invalid note passed to noteToStr.")
  }

  const toneDiatonicIndex = note[0]
  const alteration = note[1]
  const isFlat = alteration < 0
  const isSharp = alteration > 0

  let output = diatonicScale[toneDiatonicIndex]

  if (isFlat) {
    output = output + 'b'.repeat(-1 * alteration)
  } else if (isSharp) {
    output = output + '#'.repeat(alteration)
  }

  return output
}

export {
  noteFromStr,
  noteToStr
}
