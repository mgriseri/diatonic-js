import { chromaIndex } from './chromaFuncs.js'
import { scaleRules } from './constants.js'
import { addInterval, intervalFromStr, intervalToStr, findInterval } from './intervFuncs.js'
import { noteFromStr, noteToStr } from './noteFuncs.js'

const scale = (note, intervArray) => {
  return intervArray.map(i => addInterval(note, i))
}

const scaleStack = (note, intervArray) => {
  const callback = (noteArray, nextInterval) => {
    const lastNote = noteArray[noteArray.length - 1]

    return [...noteArray, addInterval(lastNote, nextInterval)]
  }

  return intervArray
    .reduce(callback, [note])
    .slice(1, intervArray.length + 1)  // remove index 0 (root note)
}

const scalePreset = (note, scaleName) => {
  return scale(
    note,
    scaleRules[scaleName].map(n => intervalFromStr(n))
  )
}

const sortNotes = (notes) => {
  let notesSorted = [...notes].sort(
    ((note, noteNext) => chromaIndex(note) > chromaIndex(noteNext))
  )

  notesSorted = notesSorted.sort(
    (note, noteNext) => note[0] > noteNext[0]
  )

  return notesSorted
}

const findTonic = (notes) => {
  const fifthCycle = ["P5", "M2", "M6", "M3", "M7", "d5"]
  const notesSorted = sortNotes(notes)

  let intervalsFound = []
  let fifthsFound = []

  // Compute every given interval
  for (let note of notesSorted) {
    let otherNotes = notesSorted.filter(
      otherNote => noteToStr(otherNote) !== noteToStr(note)
    )
    for (let otherNote of otherNotes) {
      let intervalFound = {
        note: noteToStr(note),
        otherNote: noteToStr(otherNote),
        name: intervalToStr(findInterval(note, otherNote))
      }

      // Remove redundant intervals by
      // ignoring a slice of the circle of fifth
      if (fifthCycle.includes(intervalFound.name)) {
        intervalsFound.push(intervalFound)
      }
    }
  }

  // Sort intervals according to nb of fifths required
  intervalsFound.sort(
    (firstEl, secondEl) => {
      return fifthCycle.indexOf(firstEl.name) - fifthCycle.indexOf(secondEl.name)
    }
  )

  // Return the last one unless its a diminished fifth
  let intervWithMostFifths = intervalsFound[intervalsFound.length - 1]
  let result = intervWithMostFifths.note

  if (intervWithMostFifths.name === "d5") {
    // if a diminished fifth is found,
    // the tonic is most likely the next chromatic note
    result = noteToStr(addInterval(noteFromStr(result), intervalFromStr("m2")))
  }

  return noteFromStr(result)
}

export {
  scale,
  scaleStack,
  scalePreset,
  sortNotes,
  findTonic
}
