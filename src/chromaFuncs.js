import { diatonicNotes, diatonicScale } from './constants.js'

const chromaIndex = (note) => {
  const mod = (x, n) => (x % n + n) % n
  const toneDiatonicIndex = note[0], alteration = note[1]

  const toneName = diatonicScale[toneDiatonicIndex]
  const toneChromaIndex = diatonicNotes[toneName]
  const noteChromaIndex = mod(toneChromaIndex + alteration, 12)

  return noteChromaIndex
}

const chromaIndexFrom = (baseNote, note) => {
  const x = chromaIndex(baseNote), y = chromaIndex(note)
  const output = (12 - (x - y)) % 12

  return output
}

export {
  chromaIndex,
  chromaIndexFrom
}
