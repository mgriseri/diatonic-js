import { noteFromStr } from './noteFuncs.js'
import { chromaIndex } from './chromaFuncs.js'
import { nameAtChromaIndex } from './nameFuncs.js'
import { intervalRules } from './constants.js'
import { simpleNameFlats, simpleNameSharps } from './nameFuncs.js'
//
import { noteToStr } from './noteFuncs.js'

const intervalFromStr = (str) => {
  const interval = intervalRules[str]

  if (!str || str.length > 2 || !interval ) {
    throw new Error("Invalid str passed to intervalFromStr")
  }

  return interval
}

const intervalToStr = (inter) => {
  const callback = n => {
    const isSameDiatStep = intervalRules[n][0] === inter[0]
    const isSameChromaStep = intervalRules[n][1] === inter[1]

    return isSameDiatStep && isSameChromaStep
  }

  return Object.keys(intervalRules).find(callback)
}

const addIntervalDiat = (note, inter) => {
  const toneDiatonicIndex = note[0]
  const diatonicSteps = inter[0], chromaSteps = inter[1]

  const newDiatIndex = (toneDiatonicIndex + diatonicSteps) % 7
  const newChromaIndex = (chromaIndex(note) + chromaSteps) % 12
  const newTone = [newDiatIndex, 0]
  const newNoteName = nameAtChromaIndex(newTone, newChromaIndex)
  const newNote = noteFromStr(newNoteName)

  return newNote
}

const addInterval = (note, inter) => {
  const isSharp = note[1] > 0
  const isSemitone = inter[0] === 1
  const isTone = inter[0] === 2
  const isTritone = inter[0] === 6

  let newNote = []

  if (inter.length === 2) {
    newNote = addIntervalDiat(note, inter)
  } else if (inter.length === 1) {

    let simpleNameFunc = simpleNameFlats

    if (isSharp) {
      simpleNameFunc = simpleNameSharps
    }

    if (isSemitone) {
      newNote = addIntervalDiat(note, intervalFromStr("m2"))
      newNote = noteFromStr(simpleNameFunc(newNote))
    }

    if (isTone) {
      newNote = addIntervalDiat(note, intervalFromStr("M2"))
      newNote = noteFromStr(simpleNameFunc(newNote))
    }

    if (isTritone) {
      newNote = addIntervalDiat(note, intervalFromStr("d5"))
      newNote = noteFromStr(simpleNameFunc(newNote))
    }
  }

  return newNote
}

const findInterval = (note1, note2) => {
  let diatonicSteps = note2[0] - note1[0]
  let chromaSteps = chromaIndex(note2) - chromaIndex(note1)

  if (diatonicSteps < 0) {
    diatonicSteps = 7 + diatonicSteps
  }

  if (chromaSteps < 0) {
    chromaSteps = 12 + chromaSteps
  }

  let interval = [diatonicSteps, chromaSteps]


  if (!intervalToStr(interval) && chromaSteps === 1) {
    interval = intervalFromStr("S")
  }

  if (!intervalToStr(interval) && chromaSteps === 2) {
    interval = intervalFromStr("T")
  }

  if (!intervalToStr(interval) && chromaSteps === 6) {
    interval = intervalFromStr("TT")
  }

  return interval
}

export {
  intervalFromStr,
  intervalToStr,
  addIntervalDiat,
  addInterval,
  findInterval
}
