// DIATONIC.JS TESTS
import { noteFromStr, noteToStr } from './src/diatonic.js'
import { chromaIndex, chromaIndexFrom } from './src/diatonic.js'
import { nameAtChromaIndex, simpleNameFlats, simpleNameSharps } from './src/diatonic.js'
import { intervalFromStr, intervalToStr, addInterval, findInterval } from './src/diatonic.js'
import { scale, scaleStack, scalePreset, sortNotes, findTonic } from './src/diatonic.js'
import { chordPreset } from './src/diatonic.js'

function noteFromStrTest() {
  const inputsToTest = ["C", "G", "D#", "Cb"]
  const outputsExp = [[0, 0], [4, 0], [1, 1], [0, -1]]

  for (let i = 0; i < inputsToTest.length; i++) {
    let result = noteFromStr(inputsToTest[i])
    let resultExp = outputsExp[i]
    let isCorrectDiaNb = result[0] === resultExp[0]
    let isCorrectChromaNb = result[1] === resultExp[1]
    let isPassed = isCorrectDiaNb && isCorrectChromaNb

    if (!isPassed) {
      throw new Error('Tests for noteFromStr failed!')
    }
  }
}

function noteToStrTest() {
  const inputsToTest = [[0, 0], [4, 0], [1, 1], [0, -1]]
  const outputsExp = ["C", "G", "D#", "Cb"]

  for (let i = 0; i < inputsToTest.length; i++) {
    let result = noteToStr(inputsToTest[i])
    let resultExp = outputsExp[i]
    let isPassed =  result === resultExp

    if (!isPassed) {
      throw new Error('Tests for noteToStr failed!')
    }
  }
}

function chromaIndexTest() {
  const inputsToTest = ["C", "G", "Cb", "B", "Bb"]
  const outputsExp = [0, 7, 11, 11, 10]

  for (let i = 0; i < inputsToTest.length; i++) {
    let result = chromaIndex(noteFromStr(inputsToTest[i]))
    let resultExp = outputsExp[i]
    let isPassed =  result === resultExp

    if (!isPassed) {
      throw new Error('Tests for chromaIndex failed!')
    }
  }
}

function chromaIndexFromTest() {
  var inputsToTest = ["C", "E", "G"]
  var outputsExp = [3, 7, 10]  // indexes from 'A'

  for (let i = 0; i < inputsToTest.length; i++) {
    let result = chromaIndexFrom(
      noteFromStr("A"),
      noteFromStr(inputsToTest[i])
    )
    let resultExp = outputsExp[i]
    let isPassed =  result === resultExp

    if (!isPassed) {
      throw new Error('Tests for chromaIndexFrom failed!')
    }
  }

  var inputsToTest = ["C", "E", "G"]
  var outputsExp = [1, 5, 8]  // indexes from 'B'

  for (let i = 0; i < inputsToTest.length; i++) {
    let result = chromaIndexFrom(
      noteFromStr("B"),
      noteFromStr(inputsToTest[i])
    )
    let resultExp = outputsExp[i]
    let isPassed =  result === resultExp

    if (!isPassed) {
      throw new Error(
        'Tests for chromaIndexFrom failed!'
      )
    }
  }
}

function nameAtChromaIndexTest() {
  let results = []

  results.push(
    nameAtChromaIndex(noteFromStr("C"), 1) === "C#"
  )
  results.push(
    nameAtChromaIndex(noteFromStr("A"), 8) === "Ab"
  )
  results.push(
    nameAtChromaIndex(noteFromStr("C"), 10) === "Cbb"
  )

  for (let result of results) {
    if (!result) {
      throw new Error(
        'Tests for nameAtChromaIndex failed!'
      )
    }
  }
}

function simpleNameFlatsTest() {
  const inputsToTest = ["C", "C#", "E#"]
  const outputsExp = ["C", "Db", "F"]
  for (let i = 0; i < inputsToTest.length; i++) {
    let result = simpleNameFlats(noteFromStr(inputsToTest[i])) 
    let resultExp = outputsExp[i]
    let isPassed = result ===  resultExp

    if (!isPassed) {
      throw new Error(
        'Tests for simpleNameFlats failed!'
      )
    }
  }
}

function simpleNameSharpsTest() {
  const inputsToTest = ["C", "Db", "F"]
  const outputsExp = ["C", "C#", "F"]
  for (let i = 0; i < inputsToTest.length; i++) {
    let result = simpleNameSharps(noteFromStr(inputsToTest[i])) 
    let resultExp = outputsExp[i]
    let isPassed = result ===  resultExp

    if (!isPassed) {
      throw new Error(
        'Tests for simpleNameSharps failed!'
      )
    }
  }
}

function intervalFromStrTest() {
  const inputsToTest = ["P1", "M3"]
  const outputsExp = [[0, 0], [2, 4]]

  for (let i = 0; i < inputsToTest.length; i++) {
    let result = intervalFromStr(inputsToTest[i])
    let resultExp = outputsExp[i]
    let isCorrectDiaSteps = result[0] === resultExp[0]
    let isCorrectChromaSteps = result[1] === resultExp[1]
    let isPassed = isCorrectDiaSteps && isCorrectChromaSteps

    if (!isPassed) {
      throw new Error('Tests for intervalFromStr failed!')
    }
  }
}

function intervalToStrTest() {
  const inputsToTest = [[0, 0], [2, 4]]
  const outputsExp = ["P1", "M3"]

  for (let i = 0; i < inputsToTest.length; i++) {
    let result = intervalToStr(inputsToTest[i])
    let resultExp = outputsExp[i]
    let isPassed =  result === resultExp

    if (!isPassed) {
      throw new Error('Tests for intervalToStr failed!')
    }
  }
}

function addIntervalTest() {
  var rootNoteTest = noteFromStr("C")
  var inputsToTest = ["M3", "P5", "A4"]
  var outputsExp = ["E", "G", "F#"]
  for (let i = 0; i < inputsToTest.length; i++) {
    let result = noteToStr(addInterval(rootNoteTest, intervalFromStr(inputsToTest[i])))
    let resultExp = outputsExp[i]
    if (result !== resultExp) {
      throw new Error('Tests for addInterval failed!')
    }
  }
}

function findIntervalTest() {
  let outputs = []
  outputs.push(
    intervalToStr(
      findInterval(noteFromStr("C"), noteFromStr("G"))
    )
  )
  outputs.push(
    intervalToStr(
      findInterval(noteFromStr("D"), noteFromStr("F#"))
    )
  )
  outputs.push(
    intervalToStr(
      findInterval(noteFromStr("F"), noteFromStr("Cb"))
    )
  )

  let outputsExp = ["P5", "M3", "d5"]

  for (let i = 0; i < outputs.length; i++) {
    if (outputs[i] !== outputsExp[i]) {
      throw new Error('Tests for findInterval failed!')
    }
  }
}

function scaleTest() {
  // C major arpeggio
  var rootNoteTest = noteFromStr("C")
  var scaleToTest = scale(
    rootNoteTest,
    ["P1","M3","P5"].map(n => intervalFromStr(n))
  )
  var outputsExp = ["C", "E", "G"]
  var outputs = scaleToTest.map(n => noteToStr(n))

  for (let i = 0; i < outputs.length; i++) {
    if (outputs[i] !== outputsExp[i]) {
      throw new Error('Tests for scale failed!')
    }
  }
}

function scaleStackTest() {
  // C major arpeggio
  var rootNoteTest = noteFromStr("C")
  var scaleToTest = scaleStack(
    rootNoteTest,
    ["P1", "M3", "m3"].map(n => intervalFromStr(n))
  )
  var outputsExp = ["C", "E", "G"]
  var outputs = scaleToTest.map(n => noteToStr(n))

  for (let i = 0; i < outputs.length; i++) {
    if (outputs[i] !==outputsExp[i]) {
      throw new Error('Tests for scaleStack failed!')
    }
  }
}

function scalePresetTest() {
  // C major scale
  var rootNoteTest = noteFromStr("C")
  var scaleToTest = scalePreset(
    rootNoteTest,
    "major"
  )
  var outputsExp = ["C", "D", "E", "F", "G", "A", "B"]
  var outputs = scaleToTest.map(n => noteToStr(n))

  for (let i = 0; i < outputs.length; i++) {
    if (outputs[i] !== outputsExp[i]) {
      throw new Error('Tests for scalePreset failed!')
    }
  }
}

function sortNotesTest() {
  let input = ["D", "C", "Fb", "E"]

  let output = sortNotes(
    input.map(n => noteFromStr(n))).map(n => noteToStr(n)
  )

  let outputExp = ["C", "D", "E", "Fb"]
  for (let i = 0; i < input.length; i++) {
    if (output[i] !== outputExp[i]) {
      throw new Error('Tests for sortNotes failed!')
    }
  }
}

function findTonicTest() {
  // C Major
  var inputToTest = ["C","D","E","F","G","A","B"].map(n => noteFromStr(n))
  var output = findTonic(inputToTest)
  if (noteToStr(output) !== "C") {
    throw new Error('Tests for findTonic failed!')
  }
  // C Major Penta
  var inputToTest = ["C","D","E","G","A"].map(n => noteFromStr(n))
  var output = findTonic(inputToTest)
  if (noteToStr(output) !== "C") {
    throw new Error('Tests for findTonic failed!')
  }
  // F Major
  var inputToTest = ["F","G","A","Bb","C","D","E"].map(n => noteFromStr(n))
  var output = findTonic(inputToTest)
  if (noteToStr(output) !== "F") {
    throw new Error('Tests for findTonic failed!')
  }
  // F# Major Penta
  var inputToTest = ["F#","G#","A#", "C#","D#"].map(n => noteFromStr(n))
  var output = findTonic(inputToTest)
  if (noteToStr(output) !== "F#") {
    throw new Error('Tests for findTonic failed!')
  }
  // Bb Major incomplete
  var inputToTest = ["D","Eb","F","G","A"].map(n => noteFromStr(n))
  var output = findTonic(inputToTest)
  if (noteToStr(output) !== "Bb") {
    throw new Error('Tests for findTonic failed!')
  }
  // C Lydian (ie: G Major)
  var inputToTest = ["C","D","E","F#","G","A","B"].map(n => noteFromStr(n))
  var output = findTonic(inputToTest)
  if (noteToStr(output) !== "G") {
    throw new Error('Tests for findTonic failed!')
  }
}

function chordPresetTest() {
  // C Major chord
  var output = chordPreset(noteFromStr("C"), "maj").map(
    n => noteToStr(n)
  )

  var outputExp = ["C", "E", "G"]
  for (let i = 0; i < output.length; i++) {
    if (output[i] !== outputExp[i]) {
      throw new Error('Tests for chordPreset failed!')
    }
  }
  // Ab minor chord
  var output = chordPreset(noteFromStr("Ab"), "min").map(
    n => noteToStr(n)
  )

  var outputExp = ["Ab", "Cb", "Eb"]
  for (let i = 0; i < output.length; i++) {
    if (output[i] !== outputExp[i]) {
      throw new Error('Tests for chordPreset failed!')
    }
  }
  // Bb m7b5
  var output = chordPreset(noteFromStr("Bb"), "m7b5").map(
    n => noteToStr(n)
  )

  var outputExp = ["Bb", "Db", "Fb", "Ab"]
  for (let i = 0; i < output.length; i++) {
    if (output[i] !== outputExp[i]) {
      throw new Error('Tests for chordPreset failed!')
    }
  }
  // G 7
  var output = chordPreset(noteFromStr("G"), "7").map(
    n => noteToStr(n)
  )

  var outputExp = ["G", "B", "D", "F"]
  for (let i = 0; i < output.length; i++) {
    if (output[i] !== outputExp[i]) {
      throw new Error('Tests for chordPreset failed!')
    }
  }
  // D# 7
  var output = chordPreset(noteFromStr("D#"), "7").map(
    n => noteToStr(n)
  )

  var outputExp = ["D#", "F##", "A#", "C#"]
  for (let i = 0; i < output.length; i++) {
    if (output[i] !== outputExp[i]) {
      throw new Error('Tests for chordPreset failed!')
    }
  }
}

noteFromStrTest()
noteToStrTest()
chromaIndexTest()
chromaIndexFromTest()
nameAtChromaIndexTest()
simpleNameFlatsTest()
simpleNameSharpsTest()
intervalFromStrTest()
intervalToStrTest()
addIntervalTest()
findIntervalTest()
scaleTest()
scaleStackTest()
scalePresetTest()
sortNotesTest()
findTonicTest()
chordPresetTest()
