Todo for 0.2.0:
- chordPreset (started)
- noteFreq(NOTE, OCTAVE_NB)
- findInterval (done)
- findScale (started)
- alterScale
- chord prog transpose tool
- secondary dominant / relative maj/min or other chord prog tools

Also:

- Better tests
- better docs
- better code struct (started)
- error handling
